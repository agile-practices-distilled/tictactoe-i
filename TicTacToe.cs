﻿using System.Linq;

namespace TicTacToe
{
    public class TicTacToe
    {
        private readonly string[,] _status;
        private string _lastChip;

        public TicTacToe()
        {
            _lastChip = "O";
            _status = new[,]
            {
                {"_", "_", "_"},
                {"_", "_", "_"},
                {"_", "_", "_"}
            };
        }

        public string ShowStatus()
        {
            return $"|{_status[2,0]}|{_status[2,1]}|{_status[2,2]}|" +
                   $"|{_status[1,0]}|{_status[1,1]}|{_status[1,2]}|" +
                   $"|{_status[0,0]}|{_status[0,1]}|{_status[0,2]}|";
        }

        public string InsertChip(int position)
        {
            var column = position - 1;
            var currentRow = 0;

            while (IsPositionOccupied(column, currentRow) && currentRow < 3)
            {
                currentRow++;
            }
            
            var result = InsertChipInPosition(column, currentRow);

            if (HasAnyPlayerWon(currentRow, column))
                result = "PLAYER_X_WINS!" + result;
            
            if (IsFull())
                result = "DRAW!" + result;

            return result;
        }

        private bool HasAnyPlayerWon(int currentRow, int index)
        {
            return HorizontalVictory(currentRow) ||
                   VerticalVictory(index) ||
                   DiagonalVictory();
        }

        private bool VerticalVictory(int index)
        {
            return _status[0, index] == _status[1, index] && _status[0, index] == _status[2, index];
        }

        private bool HorizontalVictory(int currentRow)
        {
            return _status[currentRow, 0] == _status[currentRow, 1] && _status[currentRow, 0] == _status[currentRow, 2];
        }

        private bool DiagonalVictory()
        {
            return _status[1,1] != "_" && _status[0,2] == _status[1,1] && _status[1,1] == _status[2,0] ||
                   _status[1,1] != "_" && _status[2,2] == _status[1,1] && _status[1,1] == _status[0,0];
        }

        private bool IsInitialStatus()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (_status[i,j] != "_")
                        return false;
                }
            }

            return true;
        }

        private bool IsFull()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (_status[i,j] == "_")
                        return false;
                }
            }

            return true;
        }

        private bool IsPositionOccupied(int index, int row)
        {
            return _status[row,index] != "_";
        }

        private string InsertChipInPosition(int index, int row)
        {
            var chip = "O";
            if (_lastChip == "O")
                chip = "X";
            
            _lastChip = chip;
            _status[row, index] = chip;
            return ShowStatus();
        }
    }
}