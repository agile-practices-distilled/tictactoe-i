using NUnit.Framework;

namespace TicTacToe
{
    public class TicTacToeShould
    {
        private TicTacToe _ticTacToe;

        [SetUp]
        public void Init()
        {
            _ticTacToe = new TicTacToe();
        }
        
        [Test]
        public void show_game_status()
        {
            string result = _ticTacToe.ShowStatus();
            
            Assert.AreEqual("" + 
                            "|_|_|_|" +
                            "|_|_|_|" +
                            "|_|_|_|", result);
        }

        [TestCase(1,"|_|_|_|" +
                    "|_|_|_|" +
                    "|X|_|_|")]
        [TestCase(2,"|_|_|_|" +
                    "|_|_|_|" +
                    "|_|X|_|")]
        [TestCase(3,"|_|_|_|" +
                    "|_|_|_|" +
                    "|_|_|X|")]
        public void insert_chip_in_first_position(int position, string expectedResult)
        {
            string result = new TicTacToe().InsertChip(position);
            
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void insert_chip_on_top_when_occupied()
        {
            _ticTacToe.InsertChip(2);
            string result = _ticTacToe.InsertChip(2);
            
            Assert.AreEqual("" +
                            "|_|_|_|" +
                            "|_|O|_|" +
                            "|_|X|_|", result);
        }

        [Test]
        public void insert_chip_three_times_same_position()
        {
            _ticTacToe.InsertChip(3);
            _ticTacToe.InsertChip(3);
            string result = _ticTacToe.InsertChip(3);
            
            Assert.AreEqual("" +
                            "|_|_|X|" +
                            "|_|_|O|" +
                            "|_|_|X|", result);
        }

        [Test]
        public void notify_victory_when_3_chips_in_a_row_vertically()
        {
            _ticTacToe.InsertChip(1);
            _ticTacToe.InsertChip(1);
            _ticTacToe.InsertChip(2);
            _ticTacToe.InsertChip(1);
            string result = _ticTacToe.InsertChip(3);
            
            Assert.AreEqual("PLAYER_X_WINS!" +
                            "|O|_|_|" +
                            "|O|_|_|" +
                            "|X|X|X|", result);
        }

        [Test]
        public void notify_victory_when_3_chips_in_a_row_horizontally()
        {
            _ticTacToe.InsertChip(1);
            _ticTacToe.InsertChip(2);
            _ticTacToe.InsertChip(1);
            _ticTacToe.InsertChip(2);
            string result = _ticTacToe.InsertChip(1);
            
            Assert.AreEqual("PLAYER_X_WINS!" +
                            "|X|_|_|" +
                            "|X|O|_|" +
                            "|X|O|_|", result);
        }

        [Test]
        public void notify_victory_when_3_chips_in_a_row_diagonally()
        {
            _ticTacToe.InsertChip(1);
            _ticTacToe.InsertChip(3);
            _ticTacToe.InsertChip(1);
            _ticTacToe.InsertChip(1);
            _ticTacToe.InsertChip(2);
            string result = _ticTacToe.InsertChip(2);
            
            Assert.AreEqual("PLAYER_X_WINS!" +
                            "|O|_|_|" +
                            "|X|O|_|" +
                            "|X|X|O|", result);
        }

        [Test]
        public void notify_draw_when_squares_filled()
        {
            _ticTacToe.InsertChip(1);
            _ticTacToe.InsertChip(2);
            _ticTacToe.InsertChip(3);
            _ticTacToe.InsertChip(1);
            _ticTacToe.InsertChip(3);
            _ticTacToe.InsertChip(2);
            _ticTacToe.InsertChip(2);
            _ticTacToe.InsertChip(3);
            string result = _ticTacToe.InsertChip(1);
            
            Assert.AreEqual("DRAW!" +
                            "|X|X|O|" +
                            "|O|O|X|" +
                            "|X|O|X|", result);
        }
    }
}