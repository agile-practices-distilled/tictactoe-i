# Smelly TicTacToe

We created a very smelly implementation of TicTacToe in our Code Smells78 open source repository. There are quite a few code smells in the implementation:
- Comments
- Data Class
- Data Clumps
- Divergent Change
- Duplicated Code
- Feature Envy
- Large Class
- Lazy Class
- Long Method
- Long Parameter List 
- Message Chain
- Primitive Obsession 
- Shotgun Surgery

Start by identifying the smells, then slowly refactor the code. Remember to keep the tests passing at all times during the refactor. It’s okay to revert back to a previous working state at any moment.